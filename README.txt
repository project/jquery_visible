jquery_visible is a Drupal wrapper around using the jquery_visible plugin, which checks to see if a DOM element is
visible in the viewport.

Installation and Setup
======================

1. Download the library from https://github.com/teamdf/jquery-visible/archive/1.1.0.zip
2. Extract the archive so that 'jquery.visible.min.js' is in sites/all/libraries/jquery-visible/
3. Enable this module